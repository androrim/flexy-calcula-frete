<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $securityContext = $this->container->get('security.authorization_checker');
        $isLogged = $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
        
        if ($isLogged) {
            return $this->redirectToRoute('transportadora_index');
        }

        return $this->render('UserBundle:Default:index.html.twig', array(
            'isLogged' => $isLogged
        ));
    }

    /**
     * @Route("/register/confirmed")
     */
    public function registeredAction()
    {
        return $this->redirectToRoute('transportadora_index');
    }

    
}
