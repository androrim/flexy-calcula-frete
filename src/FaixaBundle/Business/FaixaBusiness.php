<?php

namespace FaixaBundle\Business;

/**
 * Regra de negócio de Faixas de Entregas
 *
 * @author Leandro de Amorim <androrim@gmail.com>
 */
class FaixaBusiness
{
    private $doctrine;

    /**
     * @param \Doctrine\Bundle\DoctrineBundle\Registry $doctrine
     * @return FaixaBusiness 
     */
    public function setDoctrine(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine)
    {
        $this->doctrine = $doctrine;

        return $this;
    }

    /**
     *
     * @param \FaixaBundle\Entity\Faixa $faixa
     * @return boolean TRUE se é uma Faixa válida
     */
    public function validate(\FaixaBundle\Entity\Faixa $faixa)
    {

        return !$this->hasCepIntercession($faixa);
    }

    /**
     * @param \FaixaBundle\Entity\Faixa $faixa
     * @return boolean TRUE se houver intercecção entre as faixas de CEPs
     */
    public function hasCepIntercession(\FaixaBundle\Entity\Faixa $faixa)
    {
        $leftCompare = '(:cep_inicial >= f.cepInicial AND :cep_inicial <= f.cepFinal)';
        $rightCompare = '(:cep_final <= f.cepFinal AND :cep_final >= f.cepInicial)';

        $query = $this->doctrine
            ->getRepository('FaixaBundle:Faixa')
            ->createQueryBuilder('f')
            ->where($leftCompare . ' OR ' . $rightCompare)
            ->setParameter('cep_inicial', $faixa->getCepInicial())
            ->setParameter('cep_final', $faixa->getCepFinal())
            ->getQuery();

        return !empty($query->getResult());
    }

    /**
     * Filtra Faixa calculando prazo de entrega com base no peso do produto
     *
     * @param \FaixaBundle\Entity\Faixa $faixa
     * @param float $peso Peso do produto a ser transportado
     * @return \FaixaBundle\Entity\Faixa
     */
    public function calculaPrazoEntrega(\FaixaBundle\Entity\Faixa $faixa, $peso)
    {
        $sobrepeso = $peso - $faixa->getPesoLimite();

        if ($sobrepeso > 0) {
            // quantas vezes excedeu o peso limeite
            $partesSobrepeso = \floor($sobrepeso / $faixa->getPrazoAdicionalPeso());
            $diasAdicionais = $partesSobrepeso * $faixa->getPrazoAdicionalDias();
            $faixa->setPrazoInicial($faixa->getPrazoInicial() + $diasAdicionais);
            $faixa->setPrazoFinal($faixa->getPrazoFinal() + $diasAdicionais);
        }

        return $faixa;
    }

    /**
     * Calcula valor da entrega com base no peso do produto
     *
     * @param \FaixaBundle\Entity\Faixa $faixa
     * @param float $peso Peso do produto a ser transportado
     * @return \FaixaBundle\Entity\Faixa
     */
    public function calculaValorEntrega(\FaixaBundle\Entity\Faixa $faixa, $peso)
    {
        $sobrepeso = $peso - $faixa->getPesoLimite();
        $valorEntrega = $peso * $faixa->getValorKg();

        if ($sobrepeso > 0) {           
            $valorEntrega = ($faixa->getPesoLimite() * $faixa->getValorKg()) 
                + ($sobrepeso * $faixa->getValorKgAdicional());

        }

        $faixa->setValorEntrega($valorEntrega);

        return $faixa;
    }

    public static function cnpCustoPazo(\FaixaBundle\Entity\Faixa $a, \FaixaBundle\Entity\Faixa $b)
    {
        $igualCusto = $a->getValorEntrega() == $b->getValorEntrega();
        $igualPrazo = $a->getPrazoFinal() == $b->getPrazoFinal();
        $menorCusto = $a->getValorEntrega() < $b->getValorEntrega();
        $menorPrazo = $a->getPrazoFinal() < $b->getPrazoFinal();

        if ($igualPrazo && $igualCusto) {
            return 0;
        }

        if ($menorPrazo && $menorCusto) {
            return -1;
        }

        if ($menorCusto) {
            return -1;
        }

        if ($menorPrazo) {
            return -1;
        }

        return 1;
    }
}
