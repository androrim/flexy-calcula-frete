<?php

namespace FaixaBundle\Controller;

use FaixaBundle\Entity\Faixa;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Faixa controller.
 *
 * @Route("faixa")
 */
class FaixaController extends Controller
{
    /**
     * Lists all transportadora entities.
     *
     * @Route("/find", name="faixa_find")
     * @Method({"GET", "POST"})
     */
    public function findAction(Request $request)
    {
        $result = array();
        $busca = array(
            'cep' => '',
            'peso' => '',
        );

        if ($request->getMethod() == 'POST') {
            $number = $this->get('utils.number');
            $business = $this->get('business.faixa')->setDoctrine($this->getDoctrine());
            
            $busca['cep'] = $number->stringIntfy($request->get('cep'));
            $busca['peso'] = $request->get('peso');

            $em = $this->getDoctrine()->getManager();
            $faixas = $em->getRepository('FaixaBundle:Faixa')
                ->findByCep($busca['cep']);

            foreach($faixas as $faixa) {
                $business->calculaPrazoEntrega($faixa, $busca['peso']);
                $business->calculaValorEntrega($faixa, $busca['peso']);
                $result[] = $faixa;
            }

            usort($result, array('FaixaBundle\Business\FaixaBusiness', 'cnpCustoPazo'));
        }

        return $this->render('FaixaBundle:Faixa:find.html.twig', array(
                    'faixas' => $result,
                    'busca' => $busca,
        ));
    }

    /**
     * Creates a new faixa entity.
     *
     * @Route("/new/{transportadoraId}", name="faixa_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request, $transportadoraId)
    {
        $em = $this->getDoctrine()->getManager();
        $transportadora = $em->getRepository('TransportadoraBundle:Transportadora')->find($transportadoraId);
        $faixa = new Faixa();
        $faixa->setTransportadora($transportadora);
        $params = explode('::',$request->attributes->get('_controller'));
        $business = $this->get('business.faixa')->setDoctrine($this->getDoctrine());

        if ($request->getMethod() == 'POST') {
            $data = $request->get('faixa');
            $number = $this->get('utils.number');

            $faixa->setCepInicial($number->stringIntfy($data['cep_inicial']))
                ->setCepFinal($number->stringIntfy($data['cep_final']))
                ->setPesoLimite($number->stringFloatfy($data['peso_limite']))
                ->setValorKg($number->stringFloatfy($data['valor_kg']))
                ->setValorKgAdicional($number->stringFloatfy($data['valor_kg_adicional']))
                ->setPrazoInicial($number->stringIntfy($data['prazo_inicial']))
                ->setPrazoFinal($number->stringIntfy($data['prazo_final']))
                ->setPrazoAdicionalDias($number->stringIntfy($data['prazo_final_dias']))
                ->setPrazoAdicionalPeso($number->stringFloatfy($data['prazo_final_peso']));

            if ($business->validate($faixa)) {
                $em->persist($faixa);
                $em->flush();
            }
            else {
                die('Não passou na validação');
            }


            return $this->redirectToRoute('transportadora_show', array('id' => $transportadora->getId()));
        }

        return $this->render('FaixaBundle:Faixa:new-edit.html.twig', array(
            'faixa' => $faixa,
            'action' => end($params),
        ));
    }

    /**
     * Displays a form to edit an existing faixa entity.
     *
     * @Route("/{id}/edit/{transportadoraId}", name="faixa_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Faixa $faixa, $transportadoraId)
    {
        $em = $this->getDoctrine()->getManager();
        $transportadora = $em->getRepository('TransportadoraBundle:Transportadora')->find($transportadoraId);
        $faixa->setTransportadora($transportadora);
        $params = explode('::',$request->attributes->get('_controller'));

        if ($request->getMethod() == 'POST') {
            $data = $_POST['faixa'];
            $number = $this->get('utils.number');

            $faixa->setCepInicial($number->stringIntfy($data['cep_inicial']))
                ->setCepFinal($number->stringIntfy($data['cep_final']))
                ->setPesoLimite($number->stringFloatfy($data['peso_limite']))
                ->setValorKg($number->stringFloatfy($data['valor_kg']))
                ->setValorKgAdicional($number->stringFloatfy($data['valor_kg_adicional']))
                ->setPrazoInicial($number->stringIntfy($data['prazo_inicial']))
                ->setPrazoFinal($number->stringIntfy($data['prazo_final']))
                ->setPrazoAdicionalDias($number->stringIntfy($data['prazo_final_dias']))
                ->setPrazoAdicionalPeso($number->stringFloatfy($data['prazo_final_peso']));

            $em->flush();
        }

        return $this->render('FaixaBundle:Faixa:new-edit.html.twig', array(
            'faixa' => $faixa,
            'action' => end($params),
        ));
    }

    /**
     * Deletes a faixa entity.
     *
     * @Route("/{id}/delete/{transportadoraId}", name="faixa_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Faixa $faixa, $transportadoraId)
    {
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $em->remove($faixa);
            $em->flush();
            return $this->redirectToRoute('transportadora_show', array('id' => $transportadoraId));
        }

        return $this->render('FaixaBundle:Faixa:delete.html.twig', array(
            'faixa' => $faixa
        ));
    }
}
