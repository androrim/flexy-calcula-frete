<?php

namespace FaixaBundle\Tests\Utils;

use FaixaBundle\Utils\Number;
use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    private $number;

    public function __construct()
    {
        parent::__construct();
        $this->number = new Number();
    }

    public function testStringIntfy()
    {
        $int = $this->number->stringIntfy('234-wptz543-678@');

        $this->assertInternalType('int', $int);
        $this->assertEquals(234543678, $int);
    }

    public function testStringFloatfy()
    {
        $float = $this->number->stringFloatfy('2,99');

        $this->assertInternalType('float', $float);
        $this->assertEquals(2.99, $float);
    }
}