<?php

namespace FaixaBundle\Tests\Business;

use FaixaBundle\Business\FaixaBusiness;
use FaixaBundle\Entity\Faixa;
use PHPUnit\Framework\TestCase;

class FaixaBusinessTest extends TestCase
{
    private $business;

    public function __construct()
    {
        parent::__construct();
        $this->business = new FaixaBusiness();
    }

    public function testCalculaPrazoEntrega()
    {
        $faixa = new Faixa();
        $faixa->setPesoLimite(5)
            ->setPrazoInicial(5)
            ->setPrazoFinal(7)
            ->setPrazoAdicionalDias(1)
            ->setPrazoAdicionalPeso(5);
       
        $this->business->calculaPrazoEntrega($faixa, 10);

        $this->assertLessThan($faixa->getPrazoFinal(), 7);
    }

    public function testCalculaValorEntrega()
    {
        $faixa = new Faixa();
        $faixa->setPesoLimite(5)
            ->setValorKg(5)
            ->setValorKgAdicional(6.5);
       
        $this->business->calculaValorEntrega($faixa, 10);

        $this->assertInternalType('float', $faixa->getValorEntrega());
    }
}