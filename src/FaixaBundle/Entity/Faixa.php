<?php

namespace FaixaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Faixa
 *
 * @ORM\Table(name="faixa")
 * @ORM\Entity(repositoryClass="FaixaBundle\Repository\FaixaRepository")
 */
class Faixa
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cep_inicial", type="integer")
     * @Assert\NotBlank()
     */
    private $cepInicial;

    /**
     * @var int
     *
     * @ORM\Column(name="cep_final", type="integer")
     * @Assert\NotBlank()
     */
    private $cepFinal;

    /**
     * @var float
     *
     * @ORM\Column(name="peso_limite", type="float")
     * @Assert\NotBlank()
     */
    private $pesoLimite;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_kg", type="float")
     * @Assert\NotBlank()
     */
    private $valorKg;

    /**
     * @var float
     *
     * @ORM\Column(name="valor_kg_adicional", type="float")
     * @Assert\NotBlank()
     */
    private $valorKgAdicional;

    /**
     * @var int
     *
     * @ORM\Column(name="prazo_inicial", type="integer")
     * @Assert\NotBlank()
     */
    private $prazoInicial;

    /**
     * @var int
     *
     * @ORM\Column(name="prazo_final", type="integer")
     * @Assert\NotBlank()
     */
    private $prazoFinal;

    /**
     * @var int
     *
     * @ORM\Column(name="prazo_adicional_dias", type="integer")
     * @Assert\NotBlank()
     */
    private $prazoAdicionalDias;
   
    /**
     * @var float
     *
     * @ORM\Column(name="prazo_adicional_peso", type="float")
     * @Assert\NotBlank()
     */
    private $prazoAdicionalPeso;

    /**
     * @var float
     *
     */
    private$valorEntrega;

    /**
     * Muitas faixas para uma transportadora
     * 
     * @var Transportadora
     *
     * @ORM\ManyToOne(targetEntity="\TransportadoraBundle\Entity\Transportadora", inversedBy="faixas", fetch="EAGER")
     * @ORM\JoinColumn(name="transportadora_id", referencedColumnName="id", nullable=false)
     * 
     * @Assert\NotBlank()
     */
    private $transportadora;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cepInicial
     *
     * @param integer $cepInicial
     * @return Faixa
     */
    public function setCepInicial($cepInicial)
    {
        $this->cepInicial = $cepInicial;

        return $this;
    }

    /**
     * Get cepInicial
     *
     * @return integer 
     */
    public function getCepInicial()
    {
        return $this->cepInicial;
    }

    /**
     * Set cepFinal
     *
     * @param integer $cepFinal
     * @return Faixa
     */
    public function setCepFinal($cepFinal)
    {
        $this->cepFinal = $cepFinal;

        return $this;
    }

    /**
     * Get cepFinal
     *
     * @return integer 
     */
    public function getCepFinal()
    {
        return $this->cepFinal;
    }

    /**
     * Set pesoLimite
     *
     * @param float $pesoLimite
     * @return Faixa
     */
    public function setPesoLimite($pesoLimite)
    {
        $this->pesoLimite = $pesoLimite;

        return $this;
    }

    /**
     * Get pesoLimite
     *
     * @return float 
     */
    public function getPesoLimite()
    {
        return $this->pesoLimite;
    }

    /**
     * Set valorKg
     *
     * @param float $valorKg
     * @return Faixa
     */
    public function setValorKg($valorKg)
    {
        $this->valorKg = $valorKg;

        return $this;
    }

    /**
     * Get valorKg
     *
     * @return float 
     */
    public function getValorKg()
    {
        return $this->valorKg;
    }

    /**
     * Set valorKgAdicional
     *
     * @param float $valorKgAdicional
     * @return Faixa
     */
    public function setValorKgAdicional($valorKgAdicional)
    {
        $this->valorKgAdicional = $valorKgAdicional;

        return $this;
    }

    /**
     * Get valorKgAdicional
     *
     * @return float 
     */
    public function getValorKgAdicional()
    {
        return $this->valorKgAdicional;
    }

    /**
     * Set prazoInicial
     *
     * @param integer $prazoInicial
     * @return Faixa
     */
    public function setPrazoInicial($prazoInicial)
    {
        $this->prazoInicial = $prazoInicial;

        return $this;
    }

    /**
     * Get prazoInicial
     *
     * @return integer 
     */
    public function getPrazoInicial()
    {
        return $this->prazoInicial;
    }

    /**
     * Set prazoFinal
     *
     * @param integer $prazoFinal
     * @return Faixa
     */
    public function setPrazoFinal($prazoFinal)
    {
        $this->prazoFinal = $prazoFinal;

        return $this;
    }

    /**
     * Get prazoFinal
     *
     * @return integer 
     */
    public function getPrazoFinal()
    {
        return $this->prazoFinal;
    }

    /**
     * Set prazoAdicionalDias
     *
     * @param integer $prazoAdicionalDias
     * @return Faixa
     */
    public function setPrazoAdicionalDias($prazoAdicionalDias)
    {
        $this->prazoAdicionalDias = $prazoAdicionalDias;

        return $this;
    }

    /**
     * Get prazoAdicionalDias
     *
     * @return integer 
     */
    public function getPrazoAdicionalDias()
    {
        return $this->prazoAdicionalDias;
    }

    /**
     * Set prazoAdicionalPeso
     *
     * @param float $prazoAdicionalPeso
     * @return Faixa
     */
    public function setPrazoAdicionalPeso($prazoAdicionalPeso)
    {
        $this->prazoAdicionalPeso = $prazoAdicionalPeso;

        return $this;
    }

    /**
     * Get prazoAdicionalPeso
     *
     * @return float 
     */
    public function getPrazoAdicionalPeso()
    {
        return $this->prazoAdicionalPeso;
    }

    /**
     * Set valorEntrega
     *
     * @param float $valorEntrega
     * @return Faixa
     */
    public function setValorEntrega($valorEntrega)
    {
        $this->valorEntrega = $valorEntrega;

        return $this;
    }

    /**
     * Get valorEntrega
     *
     * @return float 
     */
    public function getValorEntrega()
    {
        return $this->valorEntrega;
    }

    /**
     * Set transportadora
     *
     * @param \TransportadoraBundle\Entity\Transportadora $transportadora
     * @return Faixa
     */
    public function setTransportadora(\TransportadoraBundle\Entity\Transportadora $transportadora)
    {
        $this->transportadora = $transportadora;

        return $this;
    }

    /**
     * Get transportadora
     *
     * @return \TransportadoraBundle\Entity\Transportadora 
     */
    public function getTransportadora()
    {
        return $this->transportadora;
    }
}
