<?php

namespace FaixaBundle\Utils;

/**
 * Utilitário para tratamento de números
 *
 * @author Leandro de Amorim <androrim@gmail.com>
 */
class Number
{
    public function stringIntfy($stringNumber)
    {
        return (int) preg_replace("/[^0-9]/", "", $stringNumber);
    }

    public function stringFloatfy($stringFloat)
    {
        $stringFloat = str_replace('.', '', $stringFloat);
        return (float) str_replace(',', '.', $stringFloat);
    }
}
