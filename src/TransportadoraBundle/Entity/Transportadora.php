<?php

namespace TransportadoraBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Transportadora
 *
 * @ORM\Table(name="transportadora")
 * @ORM\Entity(repositoryClass="TransportadoraBundle\Repository\TransportadoraRepository")
 */
class Transportadora
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var int
     *
     * @ORM\Column(name="ativa", type="integer", length=1)
     * @Assert\NotBlank()
     */
    private $ativa = 1;
    
    /**
     * @var string
     *
     * @ORM\Column(name="cnpj", type="string", length=14, unique=true)
     * @Assert\NotBlank()
     */
    private $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="nome", type="string", length=150)
     * @Assert\NotBlank()
     */
    private $nome;

    /**
     * Uma transportadora tem muitas faixas
     * 
     * @var ArrayCollection
     * 
     * @ORM\OneToMany(targetEntity="\FaixaBundle\Entity\Faixa", mappedBy="transportadora", cascade={"all"})
     */
    private $faixas;

    public function __construct()
    {
        $this->faixas = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnpj
     *
     * @param string $cnpj
     * @return Transportadora
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    /**
     * Get cnpj
     *
     * @return integer 
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Transportadora
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set faixas
     * 
     * @param \Doctrine\Common\Collections\ArrayCollection $faixas
     * @return Transportadora
     */
    public function setFaixas(array $faixas)
    {
        $this->faixas = $faixas;

        return $this;
    }

    /**
     * Get faixas
     * 
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getFaixas()
    {
        return $this->faixas;
    }


    /**
     * Add faixas
     *
     * @param \FaixaBundle\Entity\Faixa $faixas
     * @return Transportadora
     */
    public function addFaixa(\FaixaBundle\Entity\Faixa $faixas)
    {
        $this->faixas[] = $faixas;

        return $this;
    }

    /**
     * Remove faixas
     *
     * @param \FaixaBundle\Entity\Faixa $faixas
     */
    public function removeFaixa(\FaixaBundle\Entity\Faixa $faixas)
    {
        $this->faixas->removeElement($faixas);
    }

    /**
     * Set ativa
     *
     * @param int $ativa
     * @return Transportadora
     */
    public function setAtiva($ativa)
    {
        $this->ativa = $ativa;

        return $this;
    }

    /**
     * Get ativa
     *
     * @return integer 
     */
    public function getAtiva()
    {
        return $this->ativa;
    }
}
