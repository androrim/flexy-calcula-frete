<?php

namespace TransportadoraBundle\Controller;

use TransportadoraBundle\Entity\Transportadora;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Transportadora controller.
 *
 * @Route("transportadora")
 */
class TransportadoraController extends Controller
{

    /**
     * Lists all transportadora entities.
     *
     * @Route("/", name="transportadora_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $transportadoras = $em->getRepository('TransportadoraBundle:Transportadora')->findAll();

        return $this->render('TransportadoraBundle:Transportadora:index.html.twig', array(
                    'transportadoras' => $transportadoras,
        ));
    }

    /**
     * Creates a new transportadora entity.
     *
     * @Route("/new", name="transportadora_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $transportadora = new Transportadora();

        if ($request->getMethod() == 'POST') {
            $transportadora->setCnpj(preg_replace("/[^0-9]/", "", $request->get('cnpj')));
            $transportadora->setNome($request->get('nome'));

            $em = $this->getDoctrine()->getManager();
            $em->persist($transportadora);
            $em->flush();

            return $this->redirectToRoute('transportadora_show', array('id' => $transportadora->getId()));
        }

        return $this->render('TransportadoraBundle:Transportadora:new.html.twig', array(
                    'transportadora' => $transportadora
        ));
    }

    /**
     * Finds and displays a transportadora entity.
     *
     * @Route("/{id}", name="transportadora_show")
     * @Method("GET")
     */
    public function showAction(Transportadora $transportadora)
    {
        return $this->render('TransportadoraBundle:Transportadora:show.html.twig', array(
                    'transportadora' => $transportadora,
                    'faixas' => $transportadora->getFaixas()->toArray(),
        ));
    }

    /**
     * Displays a form to edit an existing transportadora entity.
     *
     * @Route("/{id}/edit", name="transportadora_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Transportadora $transportadora)
    {
        if ($request->getMethod() == 'POST') {
            $transportadora->setCnpj(preg_replace("/[^0-9]/", "", $request->get('cnpj')));
            $transportadora->setNome($request->get('nome'));
            $transportadora->setAtiva((int) $request->get('ativa'));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transportadora_edit', array('id' => $transportadora->getId()));
        }

        return $this->render('TransportadoraBundle:Transportadora:edit.html.twig', array(
                    'transportadora' => $transportadora,
                    'faixas' => $transportadora->getFaixas()->toArray()
        ));
    }

    /**
     * Deletes a transportadora entity.
     *
     * @Route("/{id}/delete", name="transportadora_delete")
     * @Method({"GET", "POST"})
     */
    public function deleteAction(Request $request, Transportadora $transportadora)
    {
        if ($request->getMethod() == 'POST') {
            $em = $this->getDoctrine()->getManager();
            $em->remove($transportadora);
            $em->flush();
            
            return $this->redirectToRoute('transportadora_index');
        }

        return $this->render('TransportadoraBundle:Transportadora:delete.html.twig', array(
            'transportadora' => $transportadora
        ));
    }

}
