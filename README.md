Desafio - Desenvolvedor PHP
===================
Sistema de cálculo de frete por faixa de CEP + peso do produto.

-------------

##Instalação
	
Este é um projeto desenvolvido com Symfony 2.8.
Para que o projeto rode com sucesso, em seu prompt de comando,  navegue até a raiz do projeto e siga rigorosamente as instruções seguintes:

###Dependências

Execute o composer:
		
	$ composer update
	
###  Banco de Dados

Ao fim da instalação das dependências começará automaticamente a configuração do banco de dados (e de email). Basta fornecer os dados solicitados para que o Symfony atualize seus arquivos de configuração. 

1.  O Symfony perguntará qual o host do banco de dados. No caso de localhost apenas tecle "Enter".

		$ database_host (127.0.0.1):

2.  Para usar a porta padrão tecle "Enter":
		
		$ database_port (null):

3. Digite o nome do banco de dados **EXISTENTE** e tecle "Enter" caso não seja "symfony":
		
		$ database_name (symfony):

4. Informe a o usuário de seu banco de dados:
	
		$ database_user (root):

5. Digite a senha do seu banco de dados:

		$ database_password (null):

6. Como este projeto não está utilizando serviço de email e não quequer maior segurança, tecle enter para dotos os demais campos para que estes sejam nulos ou padrão:

		$ mailer_transport (smtp):
		$ mailer_host (127.0.0.1):
		$ mailer_user (null):
		$ mailer_password (null):
		$ secret (ThisTokenIsNotSoSecretChangeIt):

 
7. E, finalmente, diga para o Symfony criar as tabelas em seu banco de dados digitando o seguinte comando e teclando "Enter":

		$ php app/console doctrine:schema:create

### Acessando a Aplicação

Digite em seu terminal e tecle "Enter":

	$ php app/console server:run

Por padrão será [http://127.0.0.1:8000](http://127.0.0.1:8000), então é só começar!

## Testes Unitários

**ATENÇÃO**: para rodar os teste unitários, esta aplicação requer PHPUnit 4.8 instalado.

Na raiz do projeto, em seu prompt de comando, digite o seguinte comando e tecle "Enter":

	$ phpunit -c app


### Support StackEdit

[![](https://cdn.monetizejs.com/resources/button-32.png)](https://monetizejs.com/authorize?client_id=ESTHdCYOi18iLhhO&summary=true)

  [^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.


  [1]: http://math.stackexchange.com/
  [2]: http://daringfireball.net/projects/markdown/syntax "Markdown"
  [3]: https://github.com/jmcmanus/pagedown-extra "Pagedown Extra"
  [4]: http://meta.math.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference
  [5]: https://code.google.com/p/google-code-prettify/
  [6]: http://highlightjs.org/
  [7]: http://bramp.github.io/js-sequence-diagrams/
  [8]: http://adrai.github.io/flowchart.js/
