"use strict";

function endereco_hydrate(conteudo) {
    var endereco = 'endereço não encontrado no ViaCEP';

    if (!("erro" in conteudo)) {
        endereco = conteudo.logradouro;
        endereco += ', ' + conteudo.bairro;
        endereco += ', ' + conteudo.localidade;
        endereco += ' - ' + conteudo.uf;

    }

    document.getElementById('endereco').innerHTML= endereco;
}

(function () {
    var script = document.createElement('script');
    var cep = document.getElementById('viacep').value.replace(/\D/g, '');
    
    script.src = '//viacep.com.br/ws/'+ cep + '/json/?callback=endereco_hydrate';
    document.body.appendChild(script);

})();